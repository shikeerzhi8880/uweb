### 超微浏览器: 微、威、快、高效、极致优化
[English](README.md)

[Amazon appstore](https://www.amazon.com/TorApp-Info-uweb-browser-for-geeks/dp/B098QPR6N5)
[Downloads](en/download.md)

[超微浏览器下载及使用技巧](https://uwebzh.netlify.app/zh/)
(Mirrors:
[gitlab](https://jamesfengcao.gitlab.io/uweb/zh/)
[repo](https://repo.or.cz/uweb.git/blob_plain/HEAD:/zh/index.html)
[codeberg](https://jamesfengcao.codeberg.page/zh/)
[netlify](https://uwebzh.netlify.app/zh/)
[pages](https://uwebzh.pages.dev/zh/)
[stormkit](https://uweb.stormkit.dev/zh/)
[surge](https://uweb.surge.sh/zh/)
[github](https://torappinfo.github.io/uweb/zh/)
[bitbucket](https://torappinfo.bitbucket.io/zh/)
[vercel](https://uweb-zh.vercel.app/zh/))

- 微：不到200K。
- 威：支持使用js脚本、url服务、shell命令、内部功能链接定制菜单／（新）按钮／手势; html+js应用增强为本地应用。
- 快：手机越慢，脚本插件越多越明显。
- 高效：最少的操作。
- 极致优化：
