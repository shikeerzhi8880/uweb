### 超微浏览器: 微、威、快、高效、极致优化
[English](README.md)

[Amazon appstore](https://www.amazon.com/TorApp-Info-uweb-browser-for-geeks/dp/B098QPR6N5)
[Downloads](en/download.md)

[超微浏览器下载及使用技巧](https://uwebzh.netlify.app/zh/)
(Mirrors:
[gitlab](https://jamesfengcao.gitlab.io/uweb/zh/)
[repo](https://repo.or.cz/uweb.git/blob_plain/HEAD:/zh/index.html)
[codeberg](https://jamesfengcao.codeberg.page/zh/)
[netlify](https://uwebzh.netlify.app/zh/)
[pages](https://uwebzh.pages.dev/zh/)
[stormkit](https://uweb.stormkit.dev/zh/)
[surge](https://uweb.surge.sh/zh/)
[github](https://torappinfo.github.io/uweb/zh/)
[bitbucket](https://torappinfo.bitbucket.io/zh/)
[vercel](https://uweb-zh.vercel.app/zh/))

- 微：不到200K。
- 威：支持使用js脚本、url服务、shell命令、内部功能链接定制菜单／（新）按钮／手势; html+js应用增强为本地应用。
- 快：手机越慢，脚本插件越多越明显。
- 高效：最少的操作。
- 极致优化：

#### 软件功能
朗读、文本重排、嗅探、比价、翻译、视频解析、离线保存、阅读模式(推荐与脚本激活模式配合)、网址重定向至国内、多帐号切换、网盘离线下载、文件管理、webdav/http空间备份/恢复任何指定文件集、智能翻页、目录文件传送、网站独立设置（ua，禁用脚本，无图，激活脚本）、行编辑器、代码编辑器（支持120多种语言，语法高亮、错误提示、自动提示、直接运行测试、其它网页上直接测试）。通过安装html5应用可预览／阅读本地或在线任意格式文件（pdf,djvu,epub,mdx/mdd,docx等）。

小书签(bookmarklet)再也不用手工输入，直接将小书签网站加入书签即可。运行时长按出菜单后选择“在新窗口中打开”，小书签将作用于最后一个窗口。

独创不良信息绝杀：阻止指定根域名的一切子域名、孙域名...，不良信息一网打尽。 支持根域名前缀、网址路径正则表达式。

独创脚本激活模式，脚本执行后被激活，此后自动作用于同类网址。

支持全局用户脚本/样式，可任意叠加，轻松切换。

支持网址特定脚本/样式，可轻松支持百万以上不同脚本。

支持自定义任意多个搜索引擎; 支持执行用户提供任何js文件; 支持用户定义任意多个useragent; 长按链接支持运行第三方程序; 长按链接支持运行用户js文件。

配套安装uWeb定制Termux应用，可用地址栏作图（类似matlab功能）、超级计算器（无限精度/π的前万位或更多、无限函数、无限常量）、符号演算（解方程、分解因式、求积分导数）、离线字典查找等。譬如地址栏输入sin(x)**5就直接得到函数图形。利用好第三方代码，才能真正发挥出uWeb的潜能。
